---
title: Manual integração
tags: [$soa, $aplicacao-1, $vab]
language: C#
---

# Metadados

Precisa ser o primeiro texto a ser inserido no documento, utilizamos o formato YAML para escrever os metadados.

```
---
title: Manual integração
tags: [@soa, @aplicacao-1, @vab]
language: C#
---
```

Utilizando a informação de metadado:

```
{{ title }}
```

Esse é o título {{ page.title }} do documento.

# Índice do documento

Para adicionar um índice automático ao documento, basta logo antes de iniciar a definição do conteúdo com o código:

```
[[_TOC_]]
```

# Cabeçalhos


Cada seção do documento deve ser iniciada com um título, a depender da importância desse título damos uma ênfase maior a determinado tópico. Em geral a importância dos títulos, está relacionado a hierarquia com que os títulos aparecem no documento, exemplo:

> Título Principal  
 Seção 1  
 Subseção 1  
 Seção 2  

ficaria,

```
# Título Principal  

## Seção 1

### Subseção 2

## Seção 2
```

## Opções

# Título 1
## Título 2
### Título 3
#### Título 4
##### Título 5
###### Título 6

# Citações

As citações podem ser simples, ou em bloco. As citações ajudam a incluir textos de especialistas durante a confecção do documento e também dar ênfase em algum bloco de texto.

Para citar um texto, basta escrever:

```
> E aqui vai o texto a ser citado, logo após o sinal de maior.
```
Ficaria dessa forma:
> E aqui vai o texto a ser citado, logo após o sinal de maior.

Caso deseje citar um bloco de texto, o gitlab possui uma extensão, utilizamos:

```
>>>
E aqui vai o seu bloco de texto a ser citado.

Com mais de uma linha.
>>>
```

Ficaria dessa forma:
> E aqui vai o seu bloco de texto a ser citado.    
Com mais de uma linha.

# Código fonte

Muitas vezes precisamos, apresentar um bloco de código fonte, como por exemplo, o payload de uma requisição a um webservice. Para isso utilizamos, três sinais de crase (\`\`\`) para um bloco de código e apenas uma crase (\`) para código em linha.

## Bloco de Código

```xml
<xml>
  <cliente nome="Fulano">
    <pedido id="1" total="25.00">
      <itens>
        <item nome="produto 1" quantidade="2" precoUnitario="10.00" />
        <item nome="produto 2" quantidade="1" precoUnitario="5.00" />
      </itens>
    </pedido>
  </cliente>
</xml>
```

```python
def function():
  s = "código python"
  print s
```

```java
public void funcao(String[] args) {
  String s = "código java";
  System.out.println(s)
}
```

```csharp
public void Funcao() {
  string s = "código C# .NET";
  Console.WriteLine(s);
}

```javascript
function funcao() {
  string s = "código javascript";
  console(s);
}
```

E para código em linha podemos escrever `string s = "código em linha"`.

## Ênfase no texto

Há muitas formas de dar ênfase em textos Markdown. Podemos colocar o texto em itálico, negrito, sobreescrito e também combinar esses estilos.


| Estilo | Markdown | Resultado |
| --- | --- | --- |
| Itálico | `*itálico*, ou _itálico_` | *itálico*, ou _itálico_ |
| Negrito | `**negrito**, ou __negrito__` | **negrito**, ou __negrito__ |
| Combinação | `**negrito e _itálico_**` | **negrito e _itálico_** |
| Tachado | `~~tachado~~` | ~~tachado~~ |

## Separação horizontal

Para criar uma linha de separação horizontal basta escrever três, hiféns `---` em sequência e dar um `<enter>`.

Escreveríamos dessa forma:

```
Separação horizontal com título
---
```

Ficando assim:

Separação horizontal com título
---

## Imagens

Para referenciar imagens no documento fazemos da seguinte forma:

```
![alt text](imagens/markdown/markdown-logo.png)
```

Ficando assim:

![alt text](imagens/markdown/markdown-logo.png "Logo do Markdown")

## Vídeos


```
[![alt text](https://img.youtube.com/vi/ID_DO_VIDEO_NO_YOUTUBE_AQUI/0.jpg)](https://www.youtube.com/watch?v=ID_DO_VIDEO_NO_YOUTUBE_AQUI)
```

[![alt text](https://img.youtube.com/vi/T70t3mDiwvg/0.jpg)](https://www.youtube.com/watch?v=T70t3mDiwvg)

## Quebra de linha

Uma quebra de linha é inserida no documento, quando temos dois `<enter>` pressionado, deixando uma linha em branco, separando as duas sentenças. Se usar apenas um `<enter>` a próxima sentença ainda fará parte da sentença anterior, use isso para manter longas sentenças mais fáceis de editar, sem a necessidade de usar o scroll horizontal.

```
Meu texto com várias linhas.

Após manter uma linha em branco entre as sentenças.
```

Meu texto com várias linhas.

Após manter uma linha em branco entre as sentenças.

```
Texto com várias sentenças em uma mesma linha.
Usando apenas um <enter> para separara as setenças.
```

Texto com várias sentenças em uma mesma linha.
Usando apenas um <enter> para separara as setenças.

```
Outra possibilidade para texto com várias linhas.  
É deixar dois espaços em branco ao final da primeira linha.
```

Outra possibilidade para texto com várias linhas.  
É deixar dois espaços em branco ao final da primeira linha.

## Links

```
- Este é um [link inline](https://www.google.com)
- Este é um [link para um arquivo na mesma pasta do repositório](index.md)
- Este é um [link relativo para um readme um diretório acima do que o atual](../README.md)
- Este é um [link com um texto no título](https://www.google.com "Link do Google")
```

- Este é um [link inline](https://www.google.com)
- Este é um [link para um arquivo na mesma pasta do repositório](documentacao.md)
- Este é um [link relativo para um readme um diretório acima do que o atual](../README.md)
- Este é um [link com um texto no título](https://www.google.com "Link do Google")

Ou apenas digite o link no documento, precisa conter o protocolo (http://, https://, ftp://):

https://www.google.com

## Listas

As listas podem ser criadas de forma ordenada ou sem ordem.

```
1. Primeiro item da lista ordenada
2. Um outro item
   - Sublista sem ordem.
1. A ordem dos números não importa, desde que sejam números
   1. Sublista ordenada
   1. Próximo item da sublista
4. E outro item.
```

1. Primeiro item da lista ordenada
2. Um outro item
   - Sublista sem ordem.
1. A ordem dos números não importa, desde que sejam números
   1. Sublista ordenada
   1. Próximo item da sublista
4. E outro item.

Para uma lista sem ordem, pode-se utilizar como marcador `-`, `*`, ou `+`.

```
Lista sem ordem:

- usar
- hífen

Também podem ser:

* usar
* asterísticos

E também:

+ usar
+ sinal de mais
```

Lista sem ordem:

- usar
- hífen

Também podem ser:

* usar
* asterísticos

E também:

+ usar
+ sinal de mais

Também é possível escrever parágrafos, entre os itens de uma lista ordenada.

```
1. Primeiro item ordenado

  Texto do associado ao primeiro item.
  Inclusive pode haver múltiplos parágrafos.

1. Segundo item

  Texto associado ao seguinte item.

```

1. Primeiro item ordenado

  Texto do associado ao primeiro item.  
  Inclusive pode haver múltiplos parágrafos.

1. Segundo item

  Texto associado ao seguinte item.

## Tabelas

Tabelas são construídas com o uso de "pipes" (`|`).

1. A primeira linha contém os cabeçalhos, separados por "pipes" (`|`).
2. A segunda linha separa os cabeçalhos, das células de dados e devem conter no mímimo 3 hífens.
3. A terceira linha, ou demais, contém os valores das células:
  * Os valores das células não podem estar em várias linhas, caso necessite de incluir múltiplas linhas é preciso utilizar a tag html `<br>` para quebrar uma linha.
  * A largura das colunas são flexíveis e devem ser separadas por "pipes" (`|`).
  * Podem haver células em branco.

Exemplo:

```
| cabeçalho 1 | cabeçalho 2 | cabeçalho 3 |
| --- | --- | ---------- |
| célula 1  | célula 2  | célula 3  |
| célula 4 | célula 5 é maior | célula 6 é muito maior do que as outras, mas não tem problema. Eventualmente a célula irá aumentar a sua altura para conter a informação. |
| célula 7   |          | célula 9   |
```

| cabeçalho 1 | cabeçalho 2 | cabeçalho 3 |
| --- | --- | ---------- |
| célula 1  | célula 2  | célula 3  |
| célula 4 | célula 5 é maior | célula 6 é muito maior do que as outras, mas não tem problema. Eventualmente a célula irá aumentar a sua altura para conter a informação. |
| célula 7   |          | célula 9   |

Adicionalmente, pode-se escolher o alinhamento do texto nas colunas adicionando "dois pontos" (`:`) nos lados dos “hífens” na segunda linha. Isso afeta a todas as células da coluna:

```
| Alinhado a esquerda | Centralizado | Alinhado a direita | Alinhado a esquerda | Centralizado | Alinhado a direita |
| :---         | :---:    | ---:          | :----------- | :------: | ------------: |
| célula 1       | célula 2   | célula 3        | célula 4       | célula 5   | célula 6        |
| célula 7       | célula 8   | célula 9        | célula 10      | célula 11  | célula 12       |
```

| Alinhado a esquerda | Centralizado | Alinhado a direita | Alinhado a esquerda | Centralizado | Alinhado a direita |
| :---         | :---:    | ---:          | :----------- | :------: | ------------: |
| célula 1       | célula 2   | célula 3        | célula 4       | célula 5   | célula 6        |
| célula 7       | célula 8   | célula 9        | célula 10      | célula 11  | célula 12       |

Pode-se utilizar a tag HTML `<br>` para forçar que a célula possua múltiplas linhas:

```
| Nome | Detalhes |
|------|---------|
| Item 1 | Descrição em uma linha |
| Item 2 | Esse item tem:<br>- Múltiplos itens<br>- Que podem ser listados separadamente |
```

| Nome | Detalhes |
|------|---------|
| Item 1 | Descrição em uma linha |
| Item 2 | Esse item tem:<br>- Múltiplos itens<br>- Que podem ser listados separadamente |

## Referência

https://docs.gitlab.com/ee/user/markdown.html
