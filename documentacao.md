Tags: @SOA, @teste

[[_TOC_]]

# Visão geral

## Objetivo

Obter informações corporativas, sobre funcionários, gerências e empresas do grupo.

## Lista de Métodos

- Método 1 - /metodo-1
- Método 2 - /metodo-2
- Método 3 - /metodo-3

## Diagrama
[//]: # (Opcional)

![Imagem 1](/imagens/documentacao/uml_deployment_diagram.jpg)

## Método de autenticação

HTTP Basic Authentication

Informações de usuário e senha devem ser adicionados no Header de toda requisição, os dados de usuário e senha devem ser codificados em Base64, o dado codificado precisa estar no formato **username:password**.

```
Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
```

## Protocolo de comunicação

Serviço implantado no SOA, comunicação através de SOAP.

## URL nos ambientes

- Teste - http://testes.dominio.com.br
- QA - http://qas.dominio.com.br
- Produção - http://producao.dominio.com.br

## Aplicações dependentes
[//]: # (Opcional)

| Ssitema / Características | Tipo de Integração | Gatilho | Agente | Frequência | Volume de acessos |
| --- | --- | --- | --- | --- | --- |
| Aplicação 1 | Consulta | Automático | Windows Service | Mensal | 30 |
| Aplicação 2 | Consulta | Automático | Job | 30 min | 1000 |
| Aplicação 3 | Atualização | Ação do usuário | Funcionalidade | - | 300 |

## Domínios

[//]: # (Lista de nome de empresas do SAP)
[//]: # (Categorias de serviço)
[//]: # (Tipo de operação)

Lista de nome de empresas do SAP

| Código | Descrição |
| --- | --- |
| 01 | Empresa X |
| 02 | Empresa Y |

# Métodos

## Método 1

### Objetivo

Os sistemas satélites que desejarem consultar o resultado do processamento das integrações, deverão fazer a chamada a esse WebService por essa intermédio desta operação.

Este composto BPEL é exposto através do barramento OSB, exigindo autenticação.

### Ações esperadas após a sua execução

[//]: # (Banco de dados atualizado)
[//]: # (Arquivo gerado)
[//]: # (Dados obtidos)

### Entrada

Os sistemas satélites que desejarem consultar o resultado do processamento das integrações, deverão fazer a chamada a esse WebService por essa intermédio desta operação.

Este composto BPEL é exposto através do barramento OSB, exigindo autenticação.

#### Payload
[//]: # (Opcional)

```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:con="http://www.dominio.com.br/metodo-1">
   <soapenv:Header/>
   <soapenv:Body>
      <con:processRequest>
         <con:item0>
            <con:sistema>?</con:sistema>
            <con:nmIntegracao>?</con:nmIntegracao>
            <con:referenciaRequest>?</con:referenciaRequest>
            <!--Zero or more repetitions:-->
            <con:item0>
               <con:chave_item>?</con:chave_item>
            </con:item0>
         </con:item0>
      </con:processRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

#### Dados
[//]: # (Opcional)

| Campo | Tipo | Exemplo | Descrição |
| --- | --- | --- | --- |
| Nome_Sistema | String	| SYSCOOP	| Nome do satélite que invocou o proxy. |
| Nome_Integracao	| String	| ENVIO_DE_RUBRICA	| Nome dado ao cenário de integração. |
| Data_log | Data Time | 2015-08-10T09:55:20 | Data da execução do cenário de integração. |
| Referencia | String	| 2298164	| Chave que o identifica o registro gerado pelo sistema satélite. |
| Chave_concatenada	| String	| 100000000777	| Campo de identificação de retorno para o sistema satélite. |

### Resposta

O modelo assíncrono permite que o sistema satélite receba uma confirmação do recebimento da requisição em caso de sucesso ou não:

Sucesso - OK (Chamada HTTP)

Erro	-  Em caso de erro de validação de XML no Barramento (OSB) código de erro é retornado ao satélite que fez a requisição.
Em caso de erro de regra de negócio, o erro será validado no SAP e o retorno do processamento será armazenado no Log Integrações, que deve ser consultado pelo sistema satélite.
Erros de timeout, erros de conexão, autenticação, etc, entre o sistema satélite e o barramento OSB, serão retornados ao sistema satélite e devem ser tratados pelo mesmo.
Erros de timeout, erros de conexão, autenticação, etc, entre o barramento OSB e o SAP-PI, serão retornados ao sistema satélite e devem ser tratados pelo mesmo.

#### Query
[//]: # (Opcional)

N/A

#### Tabela Banco de Dados
[//]: # (Opcional)

Banco de dados: GAB-X1234 \
Nome da tabela: Log_Integracoes

| PK | Nome do Campo | Tipo/ Tamanho | Descrição |
| --- | --- | --- | --- |
| N	| NM_SISTEMA | Varchar2(20) | Nome do sistema (Valor do elemento <NOME_SISTEMA> da tag <CONTROLE> do payload de request) |
| N | NM_INTEGRACAO | Varchar2(100) | Nome da integração. (Valor do elemento <NOME_INTEGRACAO> da tag <CONTROLE> do payload de request) |

### Cenários de Teste

#### Teste de request do sistema satélite para acessar barramento OSB com autenticação. (Erro)

| Passo |	Descrição |
| --- | --- |
| Pré – condição | Ter os request de testes dos sistemas envolvidos (satélite/SAP). O request de teste deve ser configurado com dados inválidos para a autenticação no barramento OSB. |
| 1 |	Realizar o request de teste. |
| Resultado esperado | Os dados do request feito não devem chegar ao destino configurado. Deve ser retornado uma mensagem dizendo que os dados fornecidos para a autenticação no barramento são inválidos. |

#### Teste de request do sistema satélite para acessar barramento OSB com autenticação. (Sucesso)

| Passo |	Descrição |
| --- | --- |
| Pré – condição | Ter os request de testes dos sistemas envolvidos (satélite/SAP). O request de teste deve ser configurado com dados válidos para a autenticação no barramento OSB. O barramento OSB deve ser configurado para passar dados válidos à autenticação do serviço que simula a interface do SAP. |
| 1 |	Realizar o request de teste |
| Resultado esperado | Os dados do request feito, devem chegar ao destino configurado. |

# Anexos

## WSDL

[URL](/artefatos/documentacao/arquivo.wsdl)
